// +----------------------------------------------------------------------
// | CCMiniCloud [ Cloud Framework ]
// +----------------------------------------------------------------------
// | Copyright (c) 2021 www.code942.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 明章科技
// +----------------------------------------------------------------------

/**
 * Notes: 相册模块业务逻辑
 * Ver : CCMiniCloud Framework 2.0.1 ALL RIGHTS RESERVED BY www.code942.com
 * Date: 2020-10-24 07:48:00 
 */

const BaseCCMiniService = require('./base_ccmini_service.js');
const ccminiDbUtil = require('../framework/database/ccmini_db_util.js');
const ccminiUtil = require('../framework/utils/ccmini_util.js');
const ccminiStrUtil = require('../framework/utils/ccmini_str_util.js');
const ccminiCloudUtil = require('../framework/cloud/ccmini_cloud_util.js');
const AlbumModel = require('../model/album_model.js');
const UserModel = require('../model/user_model.js');
class AlbumService extends BaseCCMiniService {

	async updateAlbumPic({
		albumId,
		imgList
	}) {

		let newList = await ccminiCloudUtil.getTempFileURL(imgList);

		let album = await AlbumModel.getOne(albumId, 'ALBUM_PIC');

		let picList = await ccminiCloudUtil.handlerCloudFiles(album.ALBUM_PIC, newList);

		let data = {
			ALBUM_PIC: picList
		};
		await AlbumModel.edit(albumId, data);

		let urls = ccminiStrUtil.getArrByKey(picList, 'url');

		return {
			urls
		};

	}

	async insertAlbum(userId, {
		title,
		content,
		type
	}) {

		let where = {
			ALBUM_TITLE: title,
			ALBUM_USER_ID: userId,
		}
		if (await AlbumModel.count(where))
			this.ccminiAppError('该标题已经存在');

		// 赋值 
		let data = {};
		data.ALBUM_TITLE = title;
		data.ALBUM_CONTENT = content;
		data.ALBUM_TYPE = type;
		data.ALBUM_DESC = ccminiStrUtil.fmtText(content, 100);


		data.ALBUM_USER_ID = userId;

		let id = await AlbumModel.insert(data);

		this.statUserAlbumCnt(userId);

		return {
			id
		};
	}

	async editAlbum(userId, {
		id,
		title,
		content,
		desc,
		type
	}) {

		let where = {
			ALBUM_TITLE: title,
			ALBUM_USER_ID: userId,
			_id: ['<>', id]
		}
		if (await AlbumModel.count(where))
			this.ccminiAppError('该标题已经存在');

		// 赋值 
		let data = {};
		data.ALBUM_TITLE = title;
		data.ALBUM_CONTENT = content;
		data.ALBUM_TYPE = type;
		data.ALBUM_DESC = ccminiStrUtil.fmtText(desc, 100);

		await AlbumModel.edit(id, data);
	}

	async delAlbum(userId, id) {
		let where = {
			ALBUM_USER_ID: userId,
			_id: id
		}

		let album = await AlbumModel.getOne(where, 'ALBUM_PIC');
		if (!album) return;

		await AlbumModel.del(where);

		let cloudIds = ccminiStrUtil.getArrByKey(album.ALBUM_PIC, 'cloudId');
		ccminiCloudUtil.deleteFiles(cloudIds);

		this.statUserAlbumCnt(userId);

		return;
	}

	async viewAlbum(id) {

		let fields = 'ALBUM_TITLE,ALBUM_CONTENT,ALBUM_TYPE,ALBUM_PIC,ALBUM_ADD_TIME,ALBUM_USER_ID,ALBUM_VIEW_CNT';

		let where = {
			_id: id,
			ALBUM_STATUS: AlbumModel.STATUS.COMM
		}
		let album = await AlbumModel.getOne(where, fields);
		if (!album) return null;

		album.USER_DETAIL = await this.getUserOne(album.ALBUM_USER_ID);

		AlbumModel.inc(id, 'ALBUM_VIEW_CNT', 1);

		return album;
	}

	async getMyAlbumDetail(userId, id) {
		let fields = 'ALBUM_TITLE,ALBUM_CONTENT,ALBUM_TYPE,ALBUM_EXPIRE_TIME,ALBUM_PIC';

		let where = {
			ALBUM_USER_ID: userId,
			_id: id
		}
		let album = await AlbumModel.getOne(where, fields);
		if (!album) return null;

		let urls = ccminiStrUtil.getArrByKey(album.ALBUM_PIC, 'url');
		album.ALBUM_PIC = urls;

		return album;
	}

	async getAlbumList({
		search,
		sortType,
		sortVal,
		orderBy,
		whereEx,
		page,
		size,
		isTotal = true,
		oldTotal
	}) {

		orderBy = orderBy || {
			'ALBUM_ORDER': 'asc',
			'ALBUM_ADD_TIME': 'desc'
		};
		let fields = 'ALBUM_TITLE,ALBUM_ADD_TIME,ALBUM_DESC,ALBUM_PIC,ALBUM_TYPE,ALBUM_VIEW_CNT,ALBUM_ORDER,' + this.getJoinUserFields();

		let where = {};
		where.ALBUM_STATUS = AlbumModel.STATUS.COMM; // 状态 

		if (ccminiUtil.isDefined(search) && search) {
			where.ALBUM_TITLE = {
				$regex: '.*' + search,
				$options: 'i'
			};
		} else if (sortType && ccminiUtil.isDefined(sortVal)) {
			switch (sortType) {
				case 'type':
					where.ALBUM_TYPE = sortVal;
					break;
				case 'sort':
					if (sortVal == 'view') {
						orderBy = {
							'ALBUM_VIEW_CNT': 'desc',
							'ALBUM_ADD_TIME': 'desc'
						};
					}
					if (sortVal == 'new') {
						orderBy = {
							'ALBUM_ADD_TIME': 'desc'
						};
					}


			}
		}
		if (whereEx && whereEx['userId'])
			where.ALBUM_USER_ID = String(whereEx['userId']);

		let joinParams = this.getJoinUserParams('ALBUM_USER_ID');
		return await ccminiDbUtil.getListJoin(AlbumModel.CL, joinParams, where, fields, orderBy, page, size, isTotal, oldTotal);
	}

	async statUserAlbumCnt(userId) {
		let where = {
			ALBUM_USER_ID: userId
		}
		let cnt = await AlbumModel.count(where);

		let whereUpdate = {
			USER_MINI_OPENID: userId
		};
		let data = {
			USER_ALBUM_CNT: cnt
		};
		await UserModel.edit(whereUpdate, data);

	}

	async getMyAlbumList(userId, {
		search,
		sortType,
		sortVal,
		orderBy,
		page,
		size,
		isTotal = true,
		oldTotal = 0
	}) {
		orderBy = orderBy || {
			'ALBUM_ADD_TIME': 'desc'
		};
		let fields = 'ALBUM_TITLE,ALBUM_ADD_TIME,ALBUM_DESC,ALBUM_PIC,ALBUM_TYPE,ALBUM_VIEW_CNT,ALBUM_ORDER';

		let where = {};
		if (ccminiUtil.isDefined(search) && search) {
			where.ALBUM_TITLE = {
				$regex: '.*' + search,
				$options: 'i'
			};
		} else if (sortType && ccminiUtil.isDefined(sortVal)) {
			switch (sortType) {
				case 'type':
					where.ALBUM_TYPE = (sortVal);
					break;
				case 'sort':
					if (sortVal == 'view') {
						orderBy = {
							'ALBUM_VIEW_CNT': 'desc',
							'ALBUM_ADD_TIME': 'desc'
						};
					}
					break;
			}
		}

		where.ALBUM_USER_ID = userId;
		return await AlbumModel.getList(where, fields, orderBy, page, size, isTotal, oldTotal);
	}
}

module.exports = AlbumService;